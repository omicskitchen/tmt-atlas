# browserify manages dependencies by compiling them into a bundle
# this allows for the use of node's "require()" in the browser 
browserify public/js/main.js -o bundle.js
# start a server
nodemon server.js