const d3 = require('d3');
const crossfilter = require('crossfilter2');
let agePlot = require('./agePlot.js');
let eduPlot = require('./eduPlot.js');
let diagPlot = require('./diagPlot.js');
let sexPlot = require('./sexPlot.js');
let pmiPlot = require('./pmiPlot.js');
let apoePlot = require('./apoePlot.js');

// Format raw data for plotting
const fmtForPlots = function formatQuantForPlots(tableData, data, key) {
  let prot = tableData.GeneName;
  let crossData = crossfilter(data);
  let geneNameDim = crossData.dimension(function (d) {
    return d.GeneName;
  });
  geneNameDim.filter(prot);
  let combined = combine(geneNameDim.top(Infinity)[0], key, tableData);

  diagPlot.plotDiag(combined);
  agePlot.plotAge(combined);
  eduPlot.plotEdu(combined);
  sexPlot.plotGender(combined);
  pmiPlot.plotPmi(combined);
  apoePlot.plotApoe(combined);
  d3.selectAll('#infoDiv, #plotCont').transition(100).style('opacity', 1);
};

const updateGeneInfo = function ({
  geneName,
  descrp,
  protId,
  uniprotId,
  table,
}) {
  document.getElementById(
    'geneTitle'
  ).innerHTML = `${geneName} | <span class="subTitle">${descrp}</span>`;

  document.getElementById(
    'uniprotId'
  ).href = `https://www.uniprot.org/uniprot/${uniprotId}`;
  document.getElementById('uniprotId').innerHTML = uniprotId;

  let keyMap = {
    padj_Dx_DEMoverN: 'Dementia over Normal',
    padj_Dx_FRLoverDEM: 'Frail over Dementia',
    padj_Dx_FRLoverN: 'Frail over Normal',
    padj_Dx_FRLoverRES: 'Frail over Resilient',
    padj_Dx_RESoverDEM: 'Resilient over Dementia',
    padj_Dx_RESoverN: 'Resilient over Normal',
    padj_age_at_death: 'Age at Death',
    padj_education: 'Years of Education',
    padj_SexM: 'SexM',
    padj_pmi: 'Post-mortem Interval',
  };

  let list = document.getElementById('sigGroupList');
  list.innerHTML = '';
  let seenSig;
  // Add significant variables to gene card
  Object.keys(table).forEach(function (key) {
    // Parse and round ints
    if (key.includes('padj')) {
      let arr = table[key].split(' ');
      let float = Number.parseFloat(arr[0]);
      let dir = arr[1];
      let dirString = dir == '↑' ? 'Increase' : 'Decrease';

      let rounded =
        float < 0.001 ? float.toExponential(2) : float.toPrecision(3);

      // Add significant variables
      if (rounded <= 0.05) {
        seenSig = true;
        let li = document.createElement('LI');
        li.classList.add('collection-item');
        li.innerHTML = `<b>${keyMap[key]}</b>: ${rounded} ${dir}`;
        list.appendChild(li);
      }
    }
  });

  if (!seenSig) {
    let li = document.createElement('LI');
    li.classList.add('collection-item');
    li.innerHTML = `No groups are significant`;
    list.appendChild(li);
  }
};

// Combine protein quantile values and sample IDs to make
// single object that holds all data
const combine = function combineRawWithKeys(data, key, tableData) {
  //console.log(data);
  //console.log(key);
  let plotObject = {
    geneName: data.GeneName,
    descrp: data.ProteinDescription,
    protId: data.ProteinID,
    uniprotId: data.UniprotAccession,
    table: tableData,
  };

  updateGeneInfo(plotObject);

  // Convert strings to ints and add quant data
  let combined = key.map((row) => {
    row.quant = +data[row.code];
    row.age_at_death = +row.age_at_death;
    row.education = +row.education;
    row.pmi = +row.pmi;
    row.apoe_genotype = row.apoe_genotype;
    return row;
  });

  plotObject.data = combined;
  return plotObject;
};

exports.fmtForPlots = fmtForPlots;
