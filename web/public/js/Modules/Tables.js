const crossfilter = require('crossfilter2');
let Plots = require('./Plots.js');
let rawData;
let sampleKey;

const setRaw = function (data) {
  rawData = data;
};

const getRaw = function () {
  return rawData;
};

const loadRaw = function () {
  const csvs = [
    './public/data/synapOnly/TMT_data_filtered_quantile_norm_synp.csv',
    './public/data/synapOnly/subsetKeySynpTypoFix.csv',
  ];

  let allData = [];
  csvs.forEach((data) => {
    let promise = d3.csv(data, (d) => d).then((data) => data);
    allData.push(promise);
  });

  return Promise.all(allData);
};

const setKey = function (key) {
  sampleKey = key;
};

const getKey = function () {
  return sampleKey;
};

const searchProt = function (prot, data) {
  let crossData = crossfilter(data);
  let geneNameDim = crossData.dimension(function (d) {
    return d.GeneName;
  });
  geneNameDim.filter(prot);
  return geneNameDim.top(Infinity);
};

// Format and display gene expression summary table
const drawTable = function (data) {
  let colNames = Object.keys(data[0]);
  //console.log(colNames);
  let columnDefs = [];
  colNames.forEach((key) => {
    let headerName = key;

    // String formatting
    if (headerName == 'GeneName') {
      headerName = 'Gene Symbol';
    } else if (headerName.includes('education')) {
      headerName = 'Edu';
    } else if (headerName.includes('padj')) {
      headerName = key.replace('padj_', '');
    }
    if (headerName.includes('Dx_')) {
      headerName = headerName.replace('Dx_', '');
    }
    if (headerName.includes('age_at_death')) {
      headerName = headerName.replace('age_at_death', 'Age at Death');
    }
    if (headerName.includes('pmi')) {
      headerName = 'PMI';
    }

    if (headerName.includes('SexM')) {
      headerName = 'SexM';
    }

    let colWidth;
    let len = headerName.length;
    let wide = window.innerWidth > 1280;

    // console.log(window.innerWidth);

    if (len == 3) {
      colWidth = wide ? 95 : 75;
    } else if (len == 7) {
      colWidth = wide ? 105 : 95;
    } else if (len == 8 || len == 9) {
      colWidth = wide ? 105 : 96;
    } else {
      colWidth = wide ? 122 : 108;
    }

    // console.log(headerName);

    let def = {
      headerName,
      field: key,
      sortable: true,
      // filter: headerName == 'Gene Name',
      // suppressSizeToFit: headerName == 'Gene Name',
      hide: key == 'UniprotAccession' || key.includes('Direction'),
      width: colWidth,
    };

    // Remove direction arrow, parse float, sort
    const pvalComp = function (a, b) {
      let float1 = parseFloat(a.split(' ')[0]);
      let float2 = parseFloat(b.split(' ')[0]);
      return float1 - float2;
    };

    if (key.includes('padj')) {
      def.comparator = pvalComp;
    }

    // Make Gene Name first column of table
    if (key == 'GeneName') {
      columnDefs.unshift(def);
    } else {
      columnDefs.push(def);
    }
  });

  let handleClick = function (event) {
    Plots.fmtForPlots(event.data, getRaw(), getKey());
  };

  let gridOptions = {
    columnDefs: columnDefs,
    rowData: data,
    defaultColDef: {
      resizable: true,
      width: 120,
    },
    onCellClicked: (event) => handleClick(event),
  };

  let eGridDiv = document.querySelector('#table');
  let grid = new agGrid.Grid(eGridDiv, gridOptions);
  // gridOptions.api.sizeColumnsToFit();
  return gridOptions;
};

exports.searchProt = searchProt;
exports.drawTable = drawTable;
exports.loadRaw = loadRaw;
exports.getRaw = getRaw;
exports.getKey = getKey;
exports.setRaw = setRaw;
exports.setKey = setKey;
