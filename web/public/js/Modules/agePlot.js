const plotAge = function (plotObj) {
  plotObj.data.sort((a, b) => d3.ascending(a.age_at_death, b.age_at_death));
  let ageDem = [];
  let ageFrl = [];
  let ageRes = [];
  let ageNorm = [];

  let norm = [];
  let frail = [];
  let dem = [];
  let res = [];

  plotObj.data.map((sample) => {
    if (sample['Project.diagnosis'] == 'N') {
      norm.push(sample.quant);
      ageNorm.push(sample.age_at_death);
    } else if (sample['Project.diagnosis'] == 'RES') {
      res.push(sample.quant);
      ageRes.push(sample.age_at_death);
    } else if (sample['Project.diagnosis'] == 'DEM_AD') {
      dem.push(sample.quant);
      ageDem.push(sample.age_at_death);
    } else if (sample['Project.diagnosis'] == 'FRL') {
      frail.push(sample.quant);
      ageFrl.push(sample.age_at_death);
    }
  });

  let trace1 = {
    name: 'Dementia_AD',
    y: dem,
    x: ageDem,
    type: 'scatter',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
    marker: {
      color: '#e66101',
    },
    mode: 'markers',
  };

  let trace2 = {
    name: 'Frail',
    y: frail,
    x: ageFrl,
    type: 'scatter',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
    marker: {
      color: '#b2abd2',
    },
    mode: 'markers',
  };

  let trace3 = {
    name: 'Normal',
    y: norm,
    x: ageNorm,
    type: 'scatter',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
    marker: {
      color: '#5e3c99',
    },
    mode: 'markers',
  };

  let trace4 = {
    x: ageRes,
    name: 'Resilient',
    y: res,
    type: 'scatter',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
    marker: {
      color: '#fdb863',
    },
    mode: 'markers',
  };

  let layout = {
    title: {
      text: `${plotObj.geneName} Abundance vs Age at Death`,
      font: {
        size: 20,
      },
      xref: 'paper',
      x: 0.0,
    },
    xaxis: {
      title: {
        text: 'Post-Mortem Interval (Hours)',
      },
      // range: [75, 104],
      // zerolinewidth: 4,
    },
    yaxis: {
      type: 'log',
      zerolinewidth: 4,
      autorange: true,
    },
  };

  let config = { responsive: true };
  let data = [trace1, trace2, trace3, trace4];

  if (!document.getElementById('ageSpan')) {
    d3.select('#plotGrid')
      .append('span')
      .attr('id', 'ageSpan')
      .classed('z-depth-2', true);
  }

  Plotly.newPlot('ageSpan', data, layout, config);
};

exports.plotAge = plotAge;
