const plotApoe = function (plotObj) {
  let two2 = [];
  let two3 = [];
  let three3 = [];
  let two4 = [];
  let three4 = [];
  let four4 = [];

  plotObj.data.sort((a, b) => d3.ascending(a.apoe_genotype, b.apoe_genotype));
  plotObj.data.map((sample) => {
    let apoe = sample.apoe_genotype;
    if (apoe == '22') {
      two2.push(sample.quant);
    } else if (apoe == '23') {
      two3.push(sample.quant);
    } else if (apoe == '24') {
      two4.push(sample.quant);
    } else if (apoe == '33') {
      three3.push(sample.quant);
    } else if (apoe == '34') {
      three4.push(sample.quant);
    } else if (apoe == '44') {
      four4.push(sample.quant);
    }
  });

  let trace1 = {
    name: '22',
    y: two2,
    type: 'box',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
    // marker: {
    //   color: '#e66101',
    // },
  };

  let trace2 = {
    name: '23',
    y: two3,
    type: 'box',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
    marker: {
      color: '#ff5c61',
    },
  };

  let trace3 = {
    name: '24',
    y: two4,
    type: 'box',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
    // marker: {
    //   color: '#fdb863',
    // },
  };

  let trace4 = {
    name: '33',
    y: three3,
    type: 'box',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
    // marker: {
    //   color: '#b2abd2',
    // },
  };

  let trace5 = {
    name: '34',
    y: three4,
    type: 'box',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
    marker: {
      color: '#ff59de',
    },
  };

  let trace6 = {
    name: '44',
    y: four4,
    type: 'box',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
    // marker: {
    //   color: '#ff5c61',
    // },
  };

  let layout = {
    title: {
      text: `${plotObj.geneName} Abundance vs ApoE Genotype`,
      font: {
        size: 18,
      },
      xref: 'paper',
      x: 0.0,
    },
    xaxis: {
      title: {
        text: 'APOE Genotype',
      },
      zerolinewidth: 4,
      type: 'category',
    },
    yaxis: {
      // title: {
      //   text: `${plotObj.geneName} Normalized Quant`,
      //   font: {
      //     family: 'Courier New, monospace',
      //     size: 20,
      //   },
      // },
      zerolinewidth: 4,
      type: 'log',
      autorange: true,
    },
    legend: {
      y: 0.5,
      font: { size: 16 },
      yref: 'paper',
    },
    showlegend: true,
  };

  let config = { responsive: true };
  let data = [trace1, trace2, trace4, trace3, trace5, trace6];

  if (!document.getElementById('apoeSpan')) {
    d3.select('#plotGrid')
      .append('span')
      .attr('id', 'apoeSpan')
      .classed('z-depth-2', true);
  }

  Plotly.newPlot('apoeSpan', data, layout, config);
};

exports.plotApoe = plotApoe;
