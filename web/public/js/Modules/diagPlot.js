const plotDiag = function (plotObj) {
  let norm = [];
  let frail = [];
  let dem = [];
  let res = [];

  plotObj.data.map((sample) => {
    if (sample['Project.diagnosis'] == 'N') {
      norm.push(sample.quant);
    } else if (sample['Project.diagnosis'] == 'RES') {
      res.push(sample.quant);
    } else if (sample['Project.diagnosis'] == 'DEM_AD') {
      dem.push(sample.quant);
    } else if (sample['Project.diagnosis'] == 'FRL') {
      frail.push(sample.quant);
    }
  });

  let trace1 = {
    name: 'Dementia_AD',
    y: dem,
    type: 'box',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
    marker: {
      color: '#e66101',
    },
  };

  let trace2 = {
    name: 'Frail',
    y: frail,
    type: 'box',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
    marker: {
      color: '#b2abd2',
    },
  };

  let trace3 = {
    name: 'Normal',
    y: norm,
    type: 'box',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
    marker: {
      color: '#5e3c99',
    },
  };

  let trace4 = {
    name: 'Resilient',
    y: res,
    type: 'box',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
    marker: {
      color: '#fdb863',
    },
  };

  let layout = {
    title: {
      text: `${plotObj.geneName} Abundance vs Diagnosis`,
      font: {
        size: 18,
      },
      xref: 'paper',
      x: 0.0,
    },
    xaxis: {
      // title: {
      //   text: 'Diagnosis Category',
      //   font: {
      //     family: 'Courier New, monospace',
      //     size: 20,
      //   },
      // },
      zerolinewidth: 4,
    },
    yaxis: {
      title: {
        text: `${plotObj.geneName} Normalized Quant`,
        font: {
          // size: 20,
        },
      },
      zerolinewidth: 4,
      type: 'log',
      autorange: true,
    },
    legend: {
      y: 0.5,
      font: { size: 16 },
      yref: 'paper',
    },
    showlegend: false,
  };

  let config = { responsive: true };
  let data = [trace1, trace2, trace3, trace4];

  if (!document.getElementById('diagSpan')) {
    d3.select('#plotGrid')
      .append('span')
      .attr('id', 'diagSpan')
      .classed('z-depth-2', true);
  }

  Plotly.newPlot('diagSpan', data, layout, config);
};

exports.plotDiag = plotDiag;
