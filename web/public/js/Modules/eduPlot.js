const plotEdu = function (plotObj) {
  let edu = [];
  let quant = [];

  plotObj.data.sort((a, b) => d3.ascending(a.edu, b.edu));
  plotObj.data.map((sample) => {
    edu.push(sample.education);
    quant.push(sample.quant);
  });

  let hs = [];
  let col = [];
  let adv = [];

  plotObj.data.map((sample) => {
    let yoe = +sample.education;
    // console.log(yoe);
    if (yoe <= 12) {
      hs.push(sample.quant);
    } else if (yoe > 12 && yoe <= 16) {
      col.push(sample.quant);
    } else if (yoe > 16) {
      adv.push(sample.quant);
    }
  });

  let trace2 = {
    name: 'Up to High School',
    y: hs,
    type: 'box',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
  };

  let trace3 = {
    name: 'HS + College',
    y: col,
    type: 'box',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
    marker: {
      color: '#ff5c61',
    },
  };

  let trace4 = {
    name: 'Adv. Degree',
    y: adv,
    type: 'box',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
    // marker: {
    //   color: '#fdb863',
    // },
  };

  let layout = {
    title: {
      text: `${plotObj.geneName} Abundance vs Years of Education`,
      font: {
        size: 18,
      },
      xref: 'paper',
      x: 0.0,
    },
    // xaxis: {
    //   title: {
    //     text: 'Years of Education',
    //     font: {
    //       family: 'Courier New, monospace',
    //       size: 20,
    //     },
    //   },
    //   zerolinewidth: 4,
    // },
    yaxis: {
      title: {
        text: `${plotObj.geneName} Normalized Quant`,
        font: {
          // size: 20,
        },
      },
      zerolinewidth: 4,
      type: 'log',
      autorange: true,
    },
    legend: {
      y: 0.5,
      font: { size: 16 },
      yref: 'paper',
    },
    showlegend: false,
  };

  let config = { responsive: true };
  let data = [trace2, trace3, trace4];

  if (!document.getElementById('eduSpan')) {
    d3.select('#plotGrid')
      .append('span')
      .attr('id', 'eduSpan')
      .classed('z-depth-2', true);
  }

  Plotly.newPlot('eduSpan', data, layout, config);
};

exports.plotEdu = plotEdu;
