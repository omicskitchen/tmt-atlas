const plotPmi = function (plotObj) {
  let pmiDem = [];
  let pmiFrl = [];
  let pmiRes = [];
  let pmiNorm = [];

  let norm = [];
  let frail = [];
  let dem = [];
  let res = [];

  plotObj.data.map((sample) => {
    if (sample['Project.diagnosis'] == 'N') {
      norm.push(sample.quant);
      pmiNorm.push(sample.age_at_death);
    } else if (sample['Project.diagnosis'] == 'RES') {
      res.push(sample.quant);
      pmiRes.push(sample.age_at_death);
    } else if (sample['Project.diagnosis'] == 'DEM_AD') {
      dem.push(sample.quant);
      pmiDem.push(sample.age_at_death);
    } else if (sample['Project.diagnosis'] == 'FRL') {
      frail.push(sample.quant);
      pmiFrl.push(sample.age_at_death);
    }
  });

  let trace1 = {
    name: 'Dementia_AD',
    y: dem,
    x: pmiDem,
    type: 'scatter',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
    marker: {
      color: '#e66101',
    },
    mode: 'markers',
  };

  let trace2 = {
    name: 'Frail',
    y: frail,
    x: pmiFrl,
    type: 'scatter',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
    marker: {
      color: '#b2abd2',
    },
    mode: 'markers',
  };

  let trace3 = {
    name: 'Normal',
    y: norm,
    x: pmiNorm,
    type: 'scatter',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
    marker: {
      color: '#5e3c99',
    },
    mode: 'markers',
  };

  let trace4 = {
    x: pmiRes,
    name: 'Resilient',
    y: res,
    type: 'scatter',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
    marker: {
      color: '#fdb863',
    },
    mode: 'markers',
  };

  let layout = {
    title: {
      text: `${plotObj.geneName} Abundance vs Post-Mortem Interval`,
      font: {
        size: 18,
      },
      xref: 'paper',
      x: 0.0,
    },
    xaxis: {
      title: {
        text: 'Post-Mortem Interval (Hours)',
      },
      // zerolinewidth: 4,
    },
    yaxis: {
      title: {
        text: `${plotObj.geneName} Normalized Quant`,
      },
      zerolinewidth: 4,
      autorange: true,
      type: 'log',
    },
  };

  let config = { responsive: true };
  let data = [trace1, trace2, trace3, trace4];

  if (!document.getElementById('pmiSpan')) {
    d3.select('#plotGrid')
      .append('span')
      .attr('id', 'pmiSpan')
      .classed('z-depth-2', true);
  }

  Plotly.newPlot('pmiSpan', data, layout, config);
};

exports.plotPmi = plotPmi;
