const plotGender = function (plotObj) {
  let maleQ = [];
  let femQ = [];

  plotObj.data.map((sample) => {
    if (sample.gender == 'M') {
      maleQ.push(sample.quant);
    } else {
      femQ.push(sample.quant);
    }
  });

  let trace1 = {
    name: 'Male',
    y: maleQ,
    type: 'box',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
  };

  let trace2 = {
    name: 'Female',
    y: femQ,
    type: 'box',
    jitter: 0.3,
    pointpos: 0,
    boxpoints: 'all',
    marker: {
      color: '#ff5c61',
    },
  };

  let layout = {
    title: {
      text: `${plotObj.geneName} Abundance vs Sex`,
      font: {
        size: 18,
      },
      xref: 'paper',
      x: 0.0,
    },
    // xaxis: {
    //   title: {
    //     text: 'Sex',
    //     font: {
    //       family: 'Courier New, monospace',
    //       size: 20,
    //     },
    //   },
    //   zerolinewidth: 4,
    // },
    yaxis: {
      // title: {
      //   text: `${plotObj.geneName} Normalized Quant`,
      //   font: {
      //     family: 'Courier New, monospace',
      //     size: 20,
      //   },
      // },
      zerolinewidth: 4,
      type: 'log',
      autorange: true,
    },
    legend: {
      y: 0.5,
      font: { size: 16 },
      yref: 'paper',
    },
    showlegend: false,
  };

  let config = { responsive: true };
  let data = [trace1, trace2];

  if (!document.getElementById('sexSpan')) {
    d3.select('#plotGrid')
      .append('span')
      .attr('id', 'sexSpan')
      .classed('z-depth-2', true);
  }

  Plotly.newPlot('sexSpan', data, layout, config);
};

exports.plotGender = plotGender;
