let d3 = require('d3');
let crossfilter = require('crossfilter2');
let Table = require('./Modules/Tables.js');
let Plots = require('./Modules/Plots.js');

let raw;
let uniqueGenes = new Set();

let geneList = document.getElementById('allGeneList');
let geneDrop = document.getElementById('allGeneInput');

const sumData = d3
  .csv('./public/data/synapOnly/TMT_summary_table_synp.csv', (row) => {
    Object.keys(row).forEach(function (key) {
      // Parse and round ints
      if (key.includes('padj')) {
        let float = Number.parseFloat(row[key]);
        let rounded =
          float < 0.001 ? float.toExponential(2) : float.toPrecision(3);
        row[key] = `${rounded}`;
      }

      if (key.includes('Direction')) {
        let dir = row[key];
        row[key] = dir == 'Increase' ? ' ↑' : ' ↓';
        let cat = key.replace('Direction_', '');
        row[`padj_${cat}`] = row[`padj_${cat}`].concat(row[key]);
      }
    });
    uniqueGenes.add(row.GeneName);
    return row;
  })
  .then((data) => {
    Table.loadRaw().then((values) => {
      //console.log(values[0]);
      Table.setRaw(values[0]);
      Table.setKey(values[1]);
      init(data);
    });
  });

function init(data) {
  // Create dropdown options for each protein
  uniqueGenes.forEach((gene) => {
    let geneOp = document.createElement('option');
    geneOp.value = gene;
    geneList.appendChild(geneOp);
  });

  let gridOptions = Table.drawTable(data);
  d3.selectAll('#table, #dropDiv').transition(100).style('opacity', 1);

  // Draw plots on dropdown select or text input
  const submitGene = function (prot) {
    if (uniqueGenes.has(prot)) {
      let tableData = Table.searchProt(prot, data)[0];
      // console.log(tableData);
      Plots.fmtForPlots(tableData, Table.getRaw(), Table.getKey());
    }
  };

  // Add dropdown options for every gene name
  geneDrop.oninput = function () {
    submitGene(this.value);
    // console.log(gridOptions);
    gridOptions.api.setQuickFilter(
      document.getElementById('allGeneInput').value
    );
  };

  // Fiter table on text input
  geneDrop.onfocus = function () {
    this.value = '';
    gridOptions.api.setQuickFilter('');
  };

  geneDrop.placeholder = 'ex: SNAP25';
  document.getElementById('load').remove();
}
