const express = require('express')
const path = require('path');

const app = express()
const port = 4001

// serves all materials in the public folder, essentiall setting "public" to "/"
// for example the "test.html" file can found at http://localhost:3001/pages/test.html
app.use(express.static('.'))

// log any funny stuff
app.listen(port, (err) => {
  if (err) {
    return console.log('Invalid request recieved:', err)
  }
  console.log(`server is listening on ${port}`)
})

// sets the default landing page
app.get('/', function (req, res) {
  //res.sendFile(path.join(__dirname + '/public/pages/index.html'));
  res.sendFile(path.join(__dirname + '/index.html'));
});

app.get('/allproteins', function (req, res) {
  //res.sendFile(path.join(__dirname + '/public/pages/index.html'));
  res.sendFile(path.join(__dirname + '/allproteins.html'));
});